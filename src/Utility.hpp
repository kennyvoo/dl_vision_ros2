/**
 * @file    Utility.hpp
 *
 * @author  btran
 *
 * @date    2020-05-04
 *
 * Copyright (c) organization
 *
 */

#pragma once

#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <math.h> 
#include "custom_msgs/msg/dl_object.hpp"
#include "custom_msgs/msg/dl_object_array.hpp"
#define PI 3.14159265
#define table_height 0.565    //change accordingly
#include <tf2/LinearMath/Quaternion.h>

namespace
{
inline std::vector<cv::Scalar> toCvScalarColors(const std::vector<std::array<int, 3>>& colors)
{
    std::vector<cv::Scalar> result;
    result.reserve(colors.size());
    std::transform(std::begin(colors), std::end(colors), std::back_inserter(result),
                   [](const auto& elem) { return cv::Scalar(elem[0], elem[1], elem[2]); });

    return result;
}

/*
cv::Point2f pixel_to_real(cv::Point pixel_coordinate,const sensor_msgs::msg::CameraInfo camera_info,const cv::Mat& depthImg){

        cv::Point2f temp;
	float ppx=camera_info.k.at(2);
	float fx = camera_info.k.at(0);
	float ppy = camera_info.k.at(5);
	float fy = camera_info.k.at(4);
	float depth= depthImg.at<unsigned short>(pixel_coordinate);
   	temp.x = (pixel_coordinate.x - ppx) / fx*depth;    
    	temp.y = (pixel_coordinate.y - ppy) / fy*depth;
	return temp;

}

void rs2_project_point_to_pixel(float pixel,const cv::Mat& depthImg, const sensor_msgs::msg::CameraInfo camera_info, const float depth)
{

    float x = 0 
    auto ppx=camera_info.k.at(2);
    auto fx = camera_info.k.at(0);
    auto ppy = camera_info.k.at(5);
    auto fy = camera_info.k.at(4);
    pixel = (distance/depth) * fx  + 2ppx ;

}
float distance_to_pixel(float distance,float depth,const sensor_msgs::msg::CameraInfo camera_info)
{


    auto fx = camera_info.k.at(0);

    return distance/depth * fx;
}
*/




int getMaxAreaContourId(std::vector<cv::Mat> contours) {
    double maxArea = 0;
    int maxAreaContourId = 999;
    for (unsigned int j = 0; j < contours.size(); j++) {
        double newArea = cv::contourArea(contours[j]);
        if (newArea > maxArea) {
            maxArea = newArea;
            maxAreaContourId = j;
        } // End if
    } // End for
    return maxAreaContourId;
} // 


inline cv::Mat visualizeOneImage(const cv::Mat& img, const std::vector<std::array<float, 4>>& bboxes,
                                 const std::vector<uint64_t>& classIndices, const std::vector<cv::Scalar> allColors,
                                 const std::vector<std::string>& allClassNames = {})
{
    assert(bboxes.size() == classIndices.size());
    if (!allClassNames.empty()) {
        assert(allClassNames.size() > *std::max_element(classIndices.begin(), classIndices.end()));
        assert(allColors.size() == allClassNames.size());
    }

    cv::Mat result = img.clone();

    for (size_t i = 0; i < bboxes.size(); ++i) {
        const auto& curBbox = bboxes[i];
        const uint64_t classIdx = classIndices[i];
        const cv::Scalar& curColor = allColors[classIdx];
        const std::string curLabel = allClassNames.empty() ? std::to_string(classIdx) : allClassNames[classIdx];

        cv::rectangle(result, cv::Point(curBbox[0], curBbox[1]), cv::Point(curBbox[2], curBbox[3]), curColor, 2);

        int baseLine = 0;
        cv::Size labelSize = cv::getTextSize(curLabel, cv::FONT_HERSHEY_COMPLEX, 0.35, 1, &baseLine);
        cv::rectangle(result, cv::Point(curBbox[0], curBbox[1]),
                      cv::Point(curBbox[0] + labelSize.width, curBbox[1] + static_cast<int>(1.3 * labelSize.height)),
                      curColor, -1);
        cv::putText(result, curLabel, cv::Point(curBbox[0], curBbox[1] + labelSize.height), cv::FONT_HERSHEY_COMPLEX,
                    0.35, cv::Scalar(255, 255, 255));
    }

    return result;
}

inline cv::Mat visualizeOneImageWithMask(const cv::Mat& img, const std::vector<std::array<float, 4>>& bboxes,    
                                         const std::vector<uint64_t>& classIndices, const std::vector<cv::Mat>& masks,
                                         const std::vector<cv::Scalar> allColors,
                                         const std::vector<std::string>& allClassNames = {},
                                         const float maskThreshold = 0.3)
{
    assert(bboxes.size() == classIndices.size());
    if (!allClassNames.empty()) {
        assert(allClassNames.size() > *std::max_element(classIndices.begin(), classIndices.end()));
        assert(allColors.size() == allClassNames.size());
    }

    cv::Mat result = img.clone();

    for (size_t i = 0; i < bboxes.size(); ++i) {

        const auto& curBbox = bboxes[i];
        const uint64_t classIdx = classIndices[i];
        cv::Mat curMask = masks[i].clone();
        const cv::Scalar& curColor = allColors[classIdx];
        const std::string curLabel = allClassNames.empty() ? std::to_string(classIdx) : allClassNames[classIdx];

        cv::rectangle(result, cv::Point(curBbox[0], curBbox[1]), cv::Point(curBbox[2], curBbox[3]), curColor, 2);

        int baseLine = 0;
        cv::Size labelSize = cv::getTextSize(curLabel, cv::FONT_HERSHEY_COMPLEX, 0.35, 1, &baseLine);
        cv::rectangle(result, cv::Point(curBbox[0], curBbox[1]),
                      cv::Point(curBbox[0] + labelSize.width, curBbox[1] + static_cast<int>(1.3 * labelSize.height)),
                      curColor, -1);
        cv::putText(result, curLabel, cv::Point(curBbox[0], curBbox[1] + labelSize.height), cv::FONT_HERSHEY_COMPLEX,
                    0.35, cv::Scalar(255, 255, 255));

        // ---------------------------------------------------------------------//
        // Visualize masks

        const cv::Rect curBoxRect(cv::Point(curBbox[0], curBbox[1]), cv::Point(curBbox[2], curBbox[3]));

        cv::resize(curMask, curMask, curBoxRect.size());

        cv::Mat finalMask = (curMask > maskThreshold);

        cv::Mat coloredRoi = (0.3 * curColor + 0.7 * result(curBoxRect));

        coloredRoi.convertTo(coloredRoi, CV_8UC3);

        std::vector<cv::Mat> contours;
        cv::Mat hierarchy;
        finalMask.convertTo(finalMask, CV_8U);

        cv::findContours(finalMask, contours, hierarchy, cv::RETR_TREE, cv::CHAIN_APPROX_SIMPLE);
        cv::drawContours(coloredRoi, contours, -1, curColor, 5, cv::LINE_8, hierarchy, 100);
        coloredRoi.copyTo(result(curBoxRect), finalMask);
    }

    return result;
}
//kenny added
inline cv::Mat visualizeOneImageWithBMask(const cv::Mat& img,const cv::Mat& depthImg,sensor_msgs::msg::CameraInfo camera_info, const std::vector<std::array<float, 4>>& bboxes,
                                         const std::vector<uint64_t>& classIndices, const std::vector<cv::Mat>& masks,
                                         const std::vector<cv::Scalar> allColors,
					custom_msgs::msg::RectOutput *rect_output,
                                         const std::vector<std::string>& allClassNames = {},
					const float maskThreshold = 0.5)
{
    assert(bboxes.size() == classIndices.size());
    if (!allClassNames.empty()) {
        assert(allClassNames.size() > *std::max_element(classIndices.begin(), classIndices.end()));
        assert(allColors.size() == allClassNames.size());
    }

    //num of objects will be equal to number of bboxes
    (*rect_output).num_objects= bboxes.size();
    cv::Mat result = img.clone();
    for (size_t i = 0; i < bboxes.size(); ++i) {

        const auto& curBbox = bboxes[i];
        const uint64_t classIdx = classIndices[i];
        cv::Mat curMask = masks[i].clone();
        const cv::Scalar& curColor = allColors[classIdx];
        const std::string curLabel = allClassNames.empty() ? std::to_string(classIdx) : allClassNames[classIdx];

        custom_msgs::msg::DlObject object;  
	object.name= curLabel;				//name of the object
	object.roi.x_offset=curBbox[0];	//top x of roi
	object.roi.y_offset=curBbox[1];	//top y of roi
	object.roi.height=curBbox[3]-curBbox[1];   //bounding box height as roi
	object.roi.width = curBbox[2]-curBbox[0];  //bounding box width as roi
        //std::cout<<curLabel<<std::endl;

        cv::rectangle(result, cv::Point(curBbox[0], curBbox[1]), cv::Point(curBbox[2], curBbox[3]), curColor, 2);

        int baseLine = 0;
        cv::Size labelSize = cv::getTextSize(curLabel, cv::FONT_HERSHEY_COMPLEX, 0.35, 1, &baseLine);
        cv::rectangle(result, cv::Point(curBbox[0], curBbox[1]),
                      cv::Point(curBbox[0] + labelSize.width, curBbox[1] + static_cast<int>(1.3 * labelSize.height)),
                     curColor, -1);
        cv::putText(result, curLabel, cv::Point(curBbox[0], curBbox[1] + labelSize.height), cv::FONT_HERSHEY_COMPLEX,
                    0.35, cv::Scalar(255, 255, 255));

        // --------------------------Visualize masks----------------------------------//
        

        const cv::Rect curBoxRect(cv::Point(curBbox[0], curBbox[1]), cv::Point(curBbox[2], curBbox[3]));

        cv::resize(curMask, curMask, curBoxRect.size());
  
        cv::Mat finalMask = (curMask > maskThreshold);

        cv::Mat coloredRoi = (0.3 * curColor + 0.7 * result(curBoxRect));

        coloredRoi.convertTo(coloredRoi, CV_8UC3);

        std::vector<cv::Mat> contours;
        cv::Mat hierarchy;
        finalMask.convertTo(finalMask, CV_8U);
        
        cv::findContours(finalMask, contours, hierarchy, cv::RETR_TREE, cv::CHAIN_APPROX_SIMPLE);
        cv::drawContours(coloredRoi, contours, -1, curColor, 5, cv::LINE_8, hierarchy, 100);

	//--------------------------Camera intrinsic parameters---------------------------//
	float ppx=camera_info.k.at(2);
        float fx = camera_info.k.at(0);
	float ppy = camera_info.k.at(5);
	float fy = camera_info.k.at(4);
	//std::cout<<"[CAMERA PARAMETER] ppx "<<ppx<<"fx "<<fx<<"ppy "<<ppy<<"fy "<<fy<<std::endl;
	
	//--------------------------get rotated rectangle and draw the major axis---------------------------//

	std::vector<cv::RotatedRect> minRect( contours.size() );
	std::vector<float> angles;
	float angle,depth;
	cv::Point pt_a,pt_b,pt_c,pt_d;
	cv::Point rotated_mid;

	//get only the largest contour (Changed according to ur use case)
	 unsigned int Maxid = getMaxAreaContourId(contours);


	// TODO:: HAVENT COMPLETE (need to fix multiple contours in a box issue)
	for( unsigned int i = 0; i < contours.size(); i++ )
	     { 
		if (i!=Maxid) 
			continue;
		//if(cv::contourArea(contours[i])<200)     //exclude the contour that has smaller area than 200
		//		continue;
	       //Function that compute rotated rectangle based on contours
		minRect[i] = cv::minAreaRect( cv::Mat(contours[i]) );    
		cv::Point2f rect_points[4];
		// 4 points of the rotated rectangle
		minRect[i].points( rect_points );
		
		//Mid points of the each side of the rotated rectangle	       	     					
		pt_a=(rect_points[0]+rect_points[3])/2;              //    0 __a__ 3
		pt_b=(rect_points[1]+rect_points[2])/2;		   //	    |  	  |
		pt_c=(rect_points[0]+rect_points[1])/2;		//	  c |     | d
		pt_d=(rect_points[3]+rect_points[2])/2;		//	    |__b__|
								//	   1       2 
		
		
		//rotated_mid= (pt_a+pt_b)/2+cv::Point(curBbox[0], curBbox[1]);  //add the top left corner to the coordinate in hte small bbox
		//std::cout<<rotated_mid;
		//For temporary, bboxes center
		rotated_mid=(cv::Point(curBbox[0], curBbox[1])+cv::Point(curBbox[2], curBbox[3]))/2;
                std::cout<<"bboxes mid "<<rotated_mid<<"mask mid "<<(pt_a+pt_b)/2+cv::Point(curBbox[0], curBbox[1])<<std::endl;


		//--------------------------Get coordinates of the object(center)---------------------------//
		//auto temp_temp =pixel_to_real(rotated_mid,camera_info,depthImg);
		//std::cout<<temp_temp<<std::endl;
		depth= depthImg.at<unsigned short>(rotated_mid)*0.001;
	   	float x = (rotated_mid.x - ppx) / fx*depth;    
	    	float y = (rotated_mid.y - ppy) / fy*depth;

                int x_axis_offset= -0.017
                int y_axis_offset= -0.01
		object.pos.position.x=x+x_axis_offset;
		object.pos.position.y=y+y_axis_offset;
		//object.pos.position.x=round(x*100)/100;
		//object.pos.position.y=round(y*100)/100;


		//Hardcode table height first

		object.pos.position.z= depth+0.005/2;      //(table_height-depth)/2 ; //center of cuboid
		//object.pos.position.z= depth+(table_height-depth)/2 ; //center of cuboid
		
		//--------------------------Get Real size and angle of object---------------------------//
		//Compare the length of 2 side of the rectangle, The longer side will be the major axis
		if(cv::norm(rect_points[0]-rect_points[1])>cv::norm(rect_points[1]-rect_points[2])){  

			cv::line( coloredRoi, pt_a, pt_b, cv::Scalar(0,0,255),2 );	//draw  the major axis(red)
			cv::line( coloredRoi, pt_c, pt_d, cv::Scalar(0,255,0),2 ); 	//minor axis (green)
			angle = round(atan2( pt_a.y - pt_b.y, pt_a.x -pt_b.x)*100)/100;			//calculate the angle
			object.length= depth *sqrt(pow((pt_a.x-pt_b.x)/fx,2)+pow((pt_a.y-pt_b.y)/fy,2) ); //calculate the length of the object
			object.breadth= depth *sqrt(pow((pt_c.x-pt_d.x)/fx,2)+pow((pt_c.y-pt_d.y)/fy,2) );   //calculate the breadth of the object

		}
		else {

			cv::line( coloredRoi, pt_c, pt_d, cv::Scalar(0,0,255),2 );	//draw  the major axis
			cv::line( coloredRoi, pt_a, pt_b, cv::Scalar(0,255,0),2 ); 	//minor axis (green)
			angle = round(atan2( pt_c.y - pt_d.y, pt_c.x -pt_d.x)*100)/100;			//calculate the angle 	
			object.breadth= depth *sqrt(pow((pt_a.x-pt_b.x)/fx,2)+pow((pt_a.y-pt_b.y)/fy,2) );
			object.length= depth *sqrt(pow((pt_c.x-pt_d.x)/fx,2)+pow((pt_c.y-pt_d.y)/fy,2) );

		}
		// height of object
		//std::cout<<table_height<<std::endl;
		object.height=0.005;       //table_height-depth;
		
		//std::cout<<rotated_mid<<std::endl;
		//mark the center point with blue dot
		cv::circle(coloredRoi, rotated_mid, 1, cv::Scalar(255, 0, 0), 1);
		
	/*
		if(depth>0){
			//std::cout<<"depth: "<<depth<<std::endl;
			//if(curLabel=="book")
			//	std::cout<<width_length<<" "<<height_length<<std::endl;
			object.rheight= depth/fx *sqrt(
			object.rheight=convertPixeltoLength(height_length,depth,true);	//convert to real height 
			object.rwidth=convertPixeltoLength(width_length,depth);		//convert to real width 
			//object.rheight=height_length* (depth*0.00163443/0.626);	
			//object.rwidth=width_length* ( depth*0.00163443/0.626);
		}
	*/
                //std::cout<<cv::norm(pt_a-pt_b)<<" "<< (object.length/depth) * fx  <<std::endl ;

		angle= angle*180/CV_PI;	
		if(angle  >0)					//anticlock wise angle
		  angle-=180;
		angle*=-1;			
		//angles.push_back(angle);			//saved multiples angles, need to postprocess this part

		cv::putText(result, cv::format("%.2f", angle), cv::Point(curBbox[0]+40, curBbox[1] + labelSize.height), cv::FONT_HERSHEY_COMPLEX,
			    0.35, cv::Scalar(0, 0, 255));
		tf2::Quaternion myQuaternion;
                myQuaternion.setRPY( 0, 0, angle*CV_PI/180 );
                //tf2::convert(myQuaternion,object.pos.orientation) not working
                object.pos.orientation.x=myQuaternion[0];
                object.pos.orientation.y=myQuaternion[1];
                object.pos.orientation.z=myQuaternion[2];
                object.pos.orientation.w=myQuaternion[3];
		//std::cout<<"quarternion "<<myQuaternion[0]<<" "<<myQuaternion[1]<<" "<<myQuaternion[2]<<" "<<myQuaternion[3]<<" from radian :"<<angle*CV_PI/180<<std::endl;
	

             	if(object.pos.position.y>0 || depth==0 ) // Skip object if position of object is below y axis(>0) or object doesnt have valid depth
                   continue;
		(*rect_output).objects.push_back(object);  //push object into the array

		}
	

		coloredRoi.copyTo(result(curBoxRect), finalMask);   
	 	(*rect_output).roi_array.push_back(object.roi);  //push each roi in sequence 

   	 }


    return result;
}







}  // namespace
