#include <chrono>
#include <functional>
#include <memory>
#include <string>
#include "rclcpp/rclcpp.hpp"
#include "sensor_msgs/msg/image.hpp"
#include "sensor_msgs/msg/camera_info.hpp"

#include <cv_bridge/cv_bridge.h>
#include "geometry_msgs/msg/pose.hpp"
#include "std_msgs/msg/string.hpp"
#include "custom_msgs/msg/dl_object.hpp"
#include "custom_msgs/msg/rect_output.hpp"

#include <ort_utility/ort_utility.hpp>
#include <onnxruntime/core/providers/cuda/cuda_provider_factory.h>
#include <onnxruntime/core/session/onnxruntime_cxx_api.h>
#include "MaskRCNN.hpp"
#include <iostream>
#include <librealsense2/rs.hpp>
#include "Utility.hpp"

#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
//#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/sync_policies/approximate_time.h>


using namespace std::chrono_literals;
using std::placeholders::_1;
using std::placeholders::_2;
static constexpr const float CONFIDENCE_THRESHOLD = 0.5;
static constexpr int64_t CUSTOM_NUM_CLASSES = 4;
static const std::vector<cv::Scalar> COLORS = toCvScalarColors(Ort::generateColorCharts(CUSTOM_NUM_CLASSES));
static const std::vector<std::string> CUSTOM_CLASSES = {"","_background_","unimate_front","unimate_back"};
static const std::string ONNX_MODEL_PATH ="/home/rosi/ros2_ws/src/vision_dl/weight/unimate_re.onnx";
//Ort::MSCOCO_NUM_CLASSES
//Ort::MSCOCO_CLASSES

class DlVision : public rclcpp::Node
{
  
  public:
    int i=0;
    DlVision():Node("DL_Vision"),
      sub_rgb_( this,"/camera/color/image_raw"),
      sub_aligned_depth_( this,"/camera/aligned_depth_to_color/image_raw"),
      sub_cam_info_(this,"/camera/color/camera_info"),
      sync_(SyncPolicy(10),sub_rgb_,sub_aligned_depth_,sub_cam_info_)
    {
    
       sync_.registerCallback(&DlVision::detect_callback, this);
       publisher_ = this->create_publisher<sensor_msgs::msg::Image>("perception/output_image", 10);
       publisher2_ = this->create_publisher<custom_msgs::msg::RectOutput>("perception/output", 10);
     }
     
     
  private:
    message_filters::Subscriber<sensor_msgs::msg::Image> sub_rgb_;
    message_filters::Subscriber<sensor_msgs::msg::Image> sub_aligned_depth_; 
    message_filters::Subscriber<sensor_msgs::msg::CameraInfo> sub_cam_info_;
    typedef message_filters::sync_policies::ApproximateTime< sensor_msgs::msg::Image, sensor_msgs::msg::Image, sensor_msgs::msg::CameraInfo > SyncPolicy;
    message_filters::Synchronizer< SyncPolicy > sync_;  
    

    cv_bridge::CvImage img_bridge;
    rclcpp::Publisher<sensor_msgs::msg::Image>::SharedPtr publisher_;
    rclcpp::Publisher<custom_msgs::msg::RectOutput>::SharedPtr publisher2_; 
  
 
    void detect_callback(const sensor_msgs::msg::Image::SharedPtr msg, const sensor_msgs::msg::Image::SharedPtr depth_msg, const sensor_msgs::msg::CameraInfo::SharedPtr camera_info) 
    {
      //RCLCPP_INFO(this->get_logger(), "Image Received");
      std::shared_ptr<cv_bridge::CvImage> imgptr = cv_bridge:: toCvCopy(msg,"bgr8");
      std::shared_ptr<cv_bridge::CvImage> depth_imageptr = cv_bridge:: toCvCopy(depth_msg, sensor_msgs::image_encodings::TYPE_16UC1);
      
      //auto start = std::chrono::high_resolution_clock::now(); 
      

      
      //----------------------Preprocess----------------------------//
     // auto start = std::chrono::high_resolution_clock::now(); 
      static float ratio =800.0 / std:: min (imgptr->image.cols,imgptr->image.rows);
      static int newW = ratio *imgptr->image.cols;
      static int newH = ratio *imgptr->image.rows;
      static  int paddedH =static_cast<int>((newH+31)/32*32);
      static int paddedW =static_cast<int>((newW+31)/32*32);
      cv::Mat tmpImg;
      cv::resize(imgptr->image, tmpImg, cv::Size(newW, newH));
      tmpImg.convertTo(tmpImg, CV_32FC3);
      tmpImg -= cv::Scalar(102.9801, 115.9465, 122.7717);  //mean_value
      cv::Mat paddedImg(paddedH, paddedW, CV_32FC3, cv::Scalar(0, 0, 0));
      
      tmpImg.copyTo(paddedImg(cv::Rect(0, 0, newW, newH)));
      
      //Create object


      static Ort::MaskRCNN *osh = new Ort::MaskRCNN(CUSTOM_NUM_CLASSES, ONNX_MODEL_PATH, 0,
        	std::vector<std::vector<int64_t>>{{Ort::MaskRCNN::IMG_CHANNEL, paddedH, paddedW}});

      osh->initClassNames(CUSTOM_CLASSES);
      static std::vector<float> dst(Ort::MaskRCNN::IMG_CHANNEL * paddedH * paddedW);
      float* tempdst= dst.data();

      osh->preprocess(tempdst, paddedImg.ptr<float>(), paddedW, paddedH, 3);	

      //auto stop = std::chrono::high_resolution_clock::now(); 
      //auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);  		
      //std::cout << "Time taken by function: "<< duration.count() << " microseconds" << std::endl; 
  

      //----------------------Detection----------------------------//	
      Ort::MaskRCNN &oshh=*osh;	
      // boxes, labels, scores, masks
      //auto start = std::chrono::high_resolution_clock::now(); 

      auto inferenceOutput = oshh({tempdst});
      //auto stop = std::chrono::high_resolution_clock::now(); 
      //auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start); 
      //std::cout << "Time taken by function: "<< duration.count() << " microseconds" << std::endl; 
      
      static std::vector<std::string> tempname=osh->classNames();


      //----------------------Post Process----------------------------//	
      custom_msgs::msg::RectOutput rect_output;
      rect_output.frame_width=imgptr->image.cols;
      rect_output.frame_height=imgptr->image.rows;
      rect_output.depth_image = *depth_msg;
      rect_output.camera_info = *camera_info;
      //cv::imwrite( "./Gray_Image.jpg", depth_imageptr->image );
      auto resultImg = processOneFrame(imgptr->image,depth_imageptr->image,*camera_info,inferenceOutput,ratio,tempname,&rect_output,0.5,true);


      //std::cout<<"hello"<<std::endl;
      
      //----------------------Debug----------------------------//
      bool debug= true;
/*    				

      auto marker_x1=425; 
      auto marker_y1=235;
      auto tempdistance1=depth_imageptr->image.at<unsigned short>(cv::Point(marker_x1,marker_y1));
      cv::circle(resultImg, cv::Point(marker_x1,marker_y1), 1, cv::Scalar(0, 0, 255), 2);
      //auto tempx1 = (marker_x1 - camera_info->k.at(2)) / camera_info->k.at(0)*tempdistance1;    //intrinsic of camera
      //auto tempy1 = (marker_y1 - camera_info->k.at(5)) / camera_info->k.at(4)*tempdistance1;
      //std::cout<<"right depth:"<<tempdistance1 <<" "<<tempx1<<" "<<tempy1<<"\n";


      auto marker_x2=325; 
      auto marker_y2=335;
      auto tempdistance2=depth_imageptr->image.at<unsigned short>(cv::Point(marker_x2,marker_y2));
      cv::circle(resultImg, cv::Point(marker_x2,marker_y2), 1, cv::Scalar(0, 0, 255), 2);
      //auto tempx2 = (marker_x2 - camera_info->k.at(2)) / camera_info->k.at(0)*tempdistance2;    //intrinsic of camera
      //auto tempy2 = (marker_y2 - camera_info->k.at(5)) / camera_info->k.at(4)*tempdistance2;
      //std::cout<<"bottom depth:"<<tempdistance2 <<" "<<tempx2<<" "<<tempy2<<"\n";
*/

      //std::cout<<"center - right: "<<cv::norm(tempx-tempx1)<<std::endl;

      //std::cout<<"center - bottom: "<<cv::norm(tempy-tempy2)<<std::endl;

     
      //float marker_x=324.3 , marker_y=234.9;
      //Center Points



      if (debug){
	      float marker_x=325 , marker_y=235;
	      auto tempdistance=depth_imageptr->image.at<unsigned short>(cv::Point(marker_x,marker_y))*0.001;
	      cv::circle(resultImg, cv::Point(marker_x,marker_y), 1, cv::Scalar(0, 0, 255), 2);
	      auto tempx = (marker_x - camera_info->k.at(2)) / camera_info->k.at(0)*tempdistance;    //intrinsic of camera
	      auto tempy = (marker_y - camera_info->k.at(5)) / camera_info->k.at(4)*tempdistance;
	      std::cout<<"center depth:"<<tempdistance <<"  Coordinate: "<<tempx<<" "<<tempy<<"\n";



	      for(unsigned int i=0;i<rect_output.objects.size();i++){
		      //std::cout<<detected_objs.objects[0].name<<"  "<<detected_objs.objects[0].pos.position.x<<"  "<<detected_objs.objects[0].pos.position.y<<"\n";

		//if(rect_output.objects[i].name!="book" && rect_output.objects[i].name!="tvmonitor")
		//	continue;
		std::cout<< rect_output.objects[i].name<<"\nBreadth: "<<rect_output.objects[i].breadth<<" Length: "<<rect_output.objects[i].length<<" Height:  "<<rect_output.objects[i].height<< "\n";
		std::cout<<"depth value : " <<rect_output.objects[i].pos.position.z<<std::endl;
		std::cout<<"angle : " <<rect_output.objects[i].pos.orientation.w<<std::endl;
		std::cout<<"coordinates : "<<rect_output.objects[i].pos.position.x<<"  "<<rect_output.objects[i].pos.position.y<<"\n\n";
		//std::cout<<"Distance from camera  	"<< cv::norm(tempx-detected_objs.objects[i].coord.x)<<" "<< cv::norm(tempy-objects_detected.objs[i].coord.y)<<std::endl;

	      }
	//------------------------------------------------------------//
 
      }

      sensor_msgs::msg::Image::SharedPtr output = cv_bridge::CvImage(std_msgs::msg::Header(), "bgr8", resultImg).toImageMsg();    
      publisher_->publish(*output);
      publisher2_->publish(rect_output);
      //auto stop = std::chrono::high_resolution_clock::now(); 
      //auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
      //float fps= duration.count()*0.000001;
      //std::cout << "fps: "<< 1.0/fps <<std::endl; 
    }
    
    
    cv::Mat processOneFrame(const cv::Mat& inputImg,const cv::Mat& depthImg,sensor_msgs::msg::CameraInfo camera_info,std::vector<std::pair<float*, std::vector<int64_t>>> inferenceOutput, float ratio, std::vector<std::string> className,custom_msgs::msg::RectOutput *rect_output, float confThresh, bool visualizeMask) const
    {
	    //custom_msgs::msg::DlObjectArray *temp= detected_objs;
	    assert(inferenceOutput[1].second.size() == 1);
	    size_t nBoxes = inferenceOutput[1].second[0];

	    std::vector<std::array<float, 4>> bboxes;
	    std::vector<uint64_t> classIndices;
	    std::vector<cv::Mat> masks;

	    bboxes.reserve(nBoxes);
	    classIndices.reserve(nBoxes);
	    masks.reserve(nBoxes);

	    for (size_t i = 0; i < nBoxes; ++i) {
	        if (inferenceOutput[2].first[i] > confThresh) {
	            //DEBUG_LOG("%f", inferenceOutput[2].first[i]);

	            float xmin = inferenceOutput[0].first[i * 4 + 0] / ratio;
	            float ymin = inferenceOutput[0].first[i * 4 + 1] / ratio;
	            float xmax = inferenceOutput[0].first[i * 4 + 2] / ratio;
	            float ymax = inferenceOutput[0].first[i * 4 + 3] / ratio;

	            xmin = std::max<float>(xmin, 0);
	            ymin = std::max<float>(ymin, 0);
	            xmax = std::min<float>(xmax, inputImg.cols);
	            ymax = std::min<float>(ymax, inputImg.rows);

	            bboxes.emplace_back(std::array<float, 4>{xmin, ymin, xmax, ymax});
	            classIndices.emplace_back(reinterpret_cast<int64_t*>(inferenceOutput[1].first)[i]);

	            cv::Mat curMask(28, 28, CV_32FC1);
	            memcpy(curMask.data, inferenceOutput[3].first + i * 28 * 28, 28 * 28 * sizeof(float));
	            masks.emplace_back(curMask);
	        }
		  }
                

		  if (bboxes.size() == 0) {
		      std::cout<<"NO DETECTION\n";
		      return inputImg;
		  }

		  return visualizeMask ? ::visualizeOneImageWithBMask(inputImg,depthImg,camera_info, bboxes, classIndices, masks, COLORS, rect_output,className)
		                       : ::visualizeOneImage(inputImg, bboxes, classIndices, COLORS, className);


	  }


 
};

int main(int argc, char * argv[])
{
  rclcpp::init(argc,argv);
  rclcpp::spin(std::make_shared<DlVision>());
  rclcpp::shutdown();
  return 0;
  
}



  // namespace
