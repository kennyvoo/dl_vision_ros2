## What is this?
This is a ros2 eloquent (c++)package that is utilizing a stereo camera and running inference on maskrcnn onnx file (with onnxruntime) to detect and locate object with output such as real life size, coordinate and angle of the object. I've used component from this [repo](https://github.com/xmba15/onnx_runtime_cpp).

**Hardware**: Realsense D415 (D400 series)

**Subscribed topics** : /camera/color/image_raw
	     	        /camera/aligned_depth_to_color/image_raw
                        /camera/color/camera_info

**Published topics** :  perception/output_image
	                perception/output


## Dependencies
1. ROS2 eloquent
2. [librealsense](https://github.com/IntelRealSense/librealsense)
3. [ROS2 intel realsense wrapper](https://github.com/intel/ros2_intel_realsense)
4. [Onnxruntime from source](https://github.com/xmba15/onnx_runtime_cpp) ,follow the guide in this repo to install it 
5. [custom messages](https://gitlab.com/kennyvoo/custom_msgs)


## Installation
```bash
cd to your ros2_ws/src
git clone https://gitlab.com/kennyvoo/dl_vision_ros2.git

colcon build at ros2_ws

```

modify   "enable_sync"  and "align_depth"  in the rs_camera.launch to true 

## How to Run?
1. launch rs_camera.launch
2. ros2 run vision_dl dl_vision



## TODO
1. Instead of fixed table_height, get the depth image of the empty field as setup. Then load it everytime when you launch the program, minus the depth from camera to object center to get the height of object.
2. Support TensorRT to speed up the inference
